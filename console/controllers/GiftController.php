<?php

namespace console\controllers;

use frontend\enums\GiftStatusEnum;
use frontend\helpers\GiftTypeHelper;
use Yii;
use yii\console\Controller;
use yii\console\Exception;
use yii\helpers\Console;
use yii\web\NotFoundHttpException;
use yii\web\UnprocessableEntityHttpException;


class GiftController extends Controller
{

	public $count;
	public $type;

	public function options($actionID)
	{
		return ['count', 'type'];
	}

	public function actionSend()
	{
		try {
			$this->type = GiftTypeHelper::typeTranslateToInt($this->type);
			$giftDataProvider = Yii::$app->gift->all(['status' => GiftStatusEnum::NEW, 'type' => $this->type], ['limit' => $this->count]);
			Yii::$app->giftPost->sendSeveral($giftDataProvider->getModels());
		} catch (NotFoundHttpException $e) {
			throw new Exception('No records to send');
		} catch (UnprocessableEntityHttpException $e) {
			throw new Exception($e);
		} catch (\Exception $e) {
			throw new Exception($e);
		}
		Console::output('sended gifts');
		foreach ($giftDataProvider->getModels() as $userGift) {
			Console::output('Gift - ' . $userGift->gift_id . ' type - ' . $userGift->gift->store->type);
		}

	}


}
