<?php

use frontend\repositories\ar\GiftYiiArRepository;
use frontend\repositories\mock\PostAppRepository;
use frontend\repositories\mock\PostBankRepository;
use frontend\repositories\mock\PostMailRepository;
use frontend\services\GiftPostService;
use frontend\services\GiftService;
use frontend\services\PostService;

$params = array_merge(
	require __DIR__ . '/../../common/config/params.php',
	require __DIR__ . '/../../common/config/params-local.php',
	require __DIR__ . '/params.php',
	require __DIR__ . '/params-local.php'
);

Yii::setAlias('@tests', dirname(__DIR__) . '/../common/tests');

return [
	'id' => 'app-console',
	'basePath' => dirname(__DIR__),
	'bootstrap' => ['log'],
	'controllerNamespace' => 'console\controllers',
	'aliases' => [
		'@bower' => '@vendor/bower-asset',
		'@npm' => '@vendor/npm-asset',
	],
	'controllerMap' => [
		'fixture' => [
			'class' => 'yii\faker\FixtureController',
			'templatePath' => '@tests/unit/fixtures/templates',
		],
	],
	'components' => [
		'log' => [
			'targets' => [
				[
					'class' => 'yii\log\FileTarget',
					'levels' => ['error', 'warning'],
				],
			],
		],
		'gift' => [
			'class' => GiftService::class,
			'repository' => new GiftYiiArRepository(),
		],
		'giftPost' => [
			'class' => GiftPostService::class,
		],
		'post' => [
			'class' => PostService::class,
			'postStrategy' =>
				[
					'giftPostSystem' => new PostMailRepository(),
					'bankBilling' => new PostBankRepository(),
					'appInterface' => new PostAppRepository()
				],
		],
	],
	'params' => $params,
];
