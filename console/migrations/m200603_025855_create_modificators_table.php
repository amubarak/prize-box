<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%type_ratio}}`.
 */
class m200603_025855_create_modificators_table extends Migration
{
	/**
	 * {@inheritdoc}
	 */
	public function safeUp()
	{
		$this->createTable('{{%type_ratio}}', [
			'id' => $this->primaryKey(),
			'from' => $this->integer()->notNull(),
			'to' => $this->integer()->notNull(),
			'ratio' => $this->double()->notNull()
		]);
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown()
	{
		$this->dropTable('{{%type_ratio}}');
	}
}
