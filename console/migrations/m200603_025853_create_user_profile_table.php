<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user_gift}}`.
 */
class m200603_025853_create_user_profile_table extends Migration
{
	/**
	 * {@inheritdoc}
	 */
	public function safeUp()
	{
		$this->createTable('{{%user_profile}}', [
			'id' => $this->primaryKey(),
			'user_id' => $this->integer()->notNull(),
			'name' => $this->integer()->notNull(),
			'second_name' => $this->integer()->notNull(),
			'address' => $this->integer()->notNull(),
		]);

		$this->addForeignKey('user_user_profile_key', 'user_profile', 'user_id', 'user', 'id');

	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown()
	{
		$this->dropTable('{{%user_profile}}');
	}
}
