<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%gift}}`.
 */
class m200603_025851_create_gift_table extends Migration
{
	/**
	 * {@inheritdoc}
	 */
	public function safeUp()
	{
		$this->createTable('{{%gift}}', [
			'id' => $this->primaryKey(),
			'name' => $this->string()->notNull(),
			'description' => $this->text(),
			'amount' => $this->float()->notNull(),
			'store_id' => $this->integer()->notNull()
		]);
		$this->addForeignKey('gift_store_key', 'gift', 'store_id', 'store', 'id');
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown()
	{
		$this->dropTable('{{%gift}}');
	}
}
