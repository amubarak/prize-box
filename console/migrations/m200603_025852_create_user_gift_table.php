<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user_gift}}`.
 */
class m200603_025852_create_user_gift_table extends Migration
{
	/**
	 * {@inheritdoc}
	 */
	public function safeUp()
	{
		$this->createTable('{{%user_gift}}', [
			'id' => $this->primaryKey(),
			'user_id' => $this->integer()->notNull(),
			'gift_id' => $this->integer()->notNull(),
			'status' => $this->integer()->notNull(),
			'create_at' => $this->timestamp()->defaultExpression('NOW()'),
			'update_at' => $this->timestamp(),
			'send_at' => $this->dateTime(),
		]);

		$this->addForeignKey('user_user_gift_key', 'user_gift', 'user_id', 'user', 'id');
		$this->addForeignKey('gift_user_gift_key', 'user_gift', 'gift_id', 'gift', 'id');

	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown()
	{
		$this->dropTable('{{%user_gift}}');
	}
}
