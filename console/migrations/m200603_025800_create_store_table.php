<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%store}}`.
 */
class m200603_025800_create_store_table extends Migration
{
	/**
	 * {@inheritdoc}
	 */
	public function safeUp()
	{
		$this->createTable('{{%store}}', [
			'id' => $this->primaryKey(),
			'type' => $this->integer()->notNull(),
			'amount' => $this->float()->notNull(),
			'update_at' => $this->timestamp(),
		]);

	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown()
	{
		$this->dropTable('{{%store}}');
	}
}
