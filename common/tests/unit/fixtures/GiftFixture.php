<?php

namespace tests\unit\fixtures;

use frontend\models\Gift;
use yii\test\ActiveFixture;

class GiftFixture extends ActiveFixture
{
	public $modelClass = Gift::class;
}