<?php

namespace tests\unit\fixtures;

use frontend\models\Ratio;
use yii\test\ActiveFixture;

class TypeRatioFixture extends ActiveFixture
{
	public $modelClass = Ratio::class;
}