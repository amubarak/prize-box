<?php

namespace tests\unit\fixtures;

use frontend\models\Store;
use yii\test\ActiveFixture;

class StoreFixture extends ActiveFixture
{
	public $modelClass = Store::class;
}