<?php

namespace tests\unit\fixtures;

use frontend\models\UserGift;
use yii\test\ActiveFixture;

class UserGiftFixture extends ActiveFixture
{
	public $modelClass = UserGift::class;
}