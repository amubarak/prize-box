<?php

return [
	'user_gift0' => [
		'user_id' => 1,
		'gift_id' => 1,
		'status' => 0,
		'create_at' => '1974-08-31T18:13:19+0300',
	],
	'user_gift1' => [
		'user_id' => 1,
		'gift_id' => 9,
		'status' => 0,
		'create_at' => '1980-03-15T20:17:46+0300',
	],
	'user_gift2' => [
		'user_id' => 1,
		'gift_id' => 7,
		'status' => 0,
		'create_at' => '1971-06-28T13:39:25+0300',
	],
	'user_gift3' => [
		'user_id' => 1,
		'gift_id' => 17,
		'status' => 0,
		'create_at' => '2004-05-13T09:24:24+0400',
	],
	'user_gift4' => [
		'user_id' => 1,
		'gift_id' => 1,
		'status' => 0,
		'create_at' => '2011-05-23T19:12:50+0400',
	],
	'user_gift5' => [
		'user_id' => 1,
		'gift_id' => 6,
		'status' => 0,
		'create_at' => '2011-12-04T13:12:30+0400',
	],
	'user_gift6' => [
		'user_id' => 1,
		'gift_id' => 6,
		'status' => 0,
		'create_at' => '2004-08-17T08:53:46+0400',
	],
	'user_gift7' => [
		'user_id' => 1,
		'gift_id' => 10,
		'status' => 0,
		'create_at' => '1979-12-07T02:12:34+0300',
	],
	'user_gift8' => [
		'user_id' => 1,
		'gift_id' => 1,
		'status' => 0,
		'create_at' => '1973-01-16T22:15:20+0300',
	],
	'user_gift9' => [
		'user_id' => 1,
		'gift_id' => 19,
		'status' => 0,
		'create_at' => '1998-10-24T22:00:25+0400',
	],
	'user_gift10' => [
		'user_id' => 1,
		'gift_id' => 8,
		'status' => 0,
		'create_at' => '1982-04-22T03:26:42+0400',
	],
	'user_gift11' => [
		'user_id' => 1,
		'gift_id' => 7,
		'status' => 0,
		'create_at' => '1993-01-14T01:14:47+0300',
	],
	'user_gift12' => [
		'user_id' => 1,
		'gift_id' => 16,
		'status' => 0,
		'create_at' => '1983-09-26T14:45:34+0400',
	],
	'user_gift13' => [
		'user_id' => 1,
		'gift_id' => 14,
		'status' => 0,
		'create_at' => '1972-09-28T13:46:33+0300',
	],
	'user_gift14' => [
		'user_id' => 1,
		'gift_id' => 11,
		'status' => 0,
		'create_at' => '2004-10-15T00:09:59+0400',
	],
	'user_gift15' => [
		'user_id' => 1,
		'gift_id' => 5,
		'status' => 0,
		'create_at' => '2009-09-13T05:07:15+0400',
	],
	'user_gift16' => [
		'user_id' => 1,
		'gift_id' => 16,
		'status' => 0,
		'create_at' => '1989-02-17T18:10:11+0300',
	],
	'user_gift17' => [
		'user_id' => 1,
		'gift_id' => 12,
		'status' => 0,
		'create_at' => '2013-03-25T20:52:20+0400',
	],
	'user_gift18' => [
		'user_id' => 1,
		'gift_id' => 4,
		'status' => 0,
		'create_at' => '1980-10-29T07:15:36+0300',
	],
	'user_gift19' => [
		'user_id' => 1,
		'gift_id' => 13,
		'status' => 0,
		'create_at' => '1978-05-07T19:56:49+0300',
	],
];
