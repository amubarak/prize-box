<?php

return [
	'store0' => [
		'amount' => 208,
		'type' => 0,
		'update_at' => '2006-01-30T00:06:56+0300',
	],
	'store1' => [
		'amount' => 631,
		'type' => 0,
		'update_at' => '2005-04-17T18:22:11+0400',
	],
	'store2' => [
		'amount' => 167,
		'type' => 1,
		'update_at' => '1994-04-21T10:19:15+0400',
	],
	'store3' => [
		'amount' => 773,
		'type' => 1,
		'update_at' => '1979-06-15T02:58:47+0300',
	],
	'store4' => [
		'amount' => 662,
		'type' => 2,
		'update_at' => '1974-11-07T22:22:25+0300',
	],
	'store5' => [
		'amount' => 228,
		'type' => 0,
		'update_at' => '2018-05-29T12:28:13+0300',
	],
	'store6' => [
		'amount' => 894,
		'type' => 2,
		'update_at' => '1989-05-22T08:25:32+0400',
	],
	'store7' => [
		'amount' => 671,
		'type' => 2,
		'update_at' => '1999-04-19T20:58:38+0400',
	],
	'store8' => [
		'amount' => 640,
		'type' => 2,
		'update_at' => '2011-06-15T16:30:51+0400',
	],
	'store9' => [
		'amount' => 135,
		'type' => 2,
		'update_at' => '1992-01-01T16:18:35+0200',
	],
	'store10' => [
		'amount' => 252,
		'type' => 1,
		'update_at' => '1995-05-10T03:59:49+0400',
	],
	'store11' => [
		'amount' => 395,
		'type' => 1,
		'update_at' => '1999-10-12T02:48:10+0400',
	],
	'store12' => [
		'amount' => 34,
		'type' => 1,
		'update_at' => '1986-01-01T03:45:12+0300',
	],
	'store13' => [
		'amount' => 726,
		'type' => 2,
		'update_at' => '2015-05-13T20:41:39+0300',
	],
	'store14' => [
		'amount' => 779,
		'type' => 0,
		'update_at' => '1972-07-10T18:23:32+0300',
	],
	'store15' => [
		'amount' => 617,
		'type' => 2,
		'update_at' => '2010-12-20T09:37:51+0300',
	],
	'store16' => [
		'amount' => 89,
		'type' => 2,
		'update_at' => '1988-11-10T13:54:19+0300',
	],
	'store17' => [
		'amount' => 230,
		'type' => 0,
		'update_at' => '1995-08-03T19:02:03+0400',
	],
	'store18' => [
		'amount' => 290,
		'type' => 2,
		'update_at' => '1974-04-19T03:32:44+0300',
	],
	'store19' => [
		'amount' => 223,
		'type' => 1,
		'update_at' => '1980-07-10T12:11:12+0300',
	],
];
