<?php

return [
	'gift0' => [
		'name' => 'Tremblayborough',
		'description' => 'Corporis consectetur ut provident aut. Dolorem aliquam dolorum aspernatur porro et.',
		'store_id' => 4,
		'amount' => 28,
	],
	'gift1' => [
		'name' => 'Lake Verdatown',
		'description' => 'Nobis consequuntur et voluptas voluptas. Rerum qui quos neque aperiam ipsa voluptas. Facere enim ea est qui quam. Sed asperiores asperiores dolor cumque omnis.',
		'store_id' => 1,
		'amount' => 43,
	],
	'gift2' => [
		'name' => 'Port Rafaelborough',
		'description' => 'Et aut qui voluptatibus nobis laborum id. Animi minima repellendus tempore. Natus illum provident sequi porro optio. Et omnis neque quos et veniam. Repellendus exercitationem laborum eos est ut.',
		'store_id' => 2,
		'amount' => 13,
	],
	'gift3' => [
		'name' => 'Stoltenbergberg',
		'description' => 'Eveniet corporis unde quam et esse. Vel qui debitis minima. Veniam veritatis quod quae sint saepe laborum nostrum.',
		'store_id' => 6,
		'amount' => 38,
	],
	'gift4' => [
		'name' => 'West Marcelino',
		'description' => 'Vitae quis commodi optio enim voluptatum. Unde doloribus exercitationem dolore voluptatem quis. Quas eius nisi quod atque. Voluptatem incidunt ex excepturi.',
		'store_id' => 16,
		'amount' => 20,
	],
	'gift5' => [
		'name' => 'Sawaynfurt',
		'description' => 'Magnam laudantium quasi sint quia laudantium necessitatibus. Qui qui praesentium aut et impedit et debitis. Facere modi nulla quasi est.',
		'store_id' => 7,
		'amount' => 19,
	],
	'gift6' => [
		'name' => 'West Antwanmouth',
		'description' => 'Et laborum voluptas quis quod aut ipsa mollitia. Et iusto ullam magnam et et possimus sint. Cum doloribus quaerat qui qui.',
		'store_id' => 9,
		'amount' => 37,
	],
	'gift7' => [
		'name' => 'Lake Estellaland',
		'description' => 'Officiis quia nulla et. Suscipit a placeat iste aspernatur perferendis omnis sed et. Et cumque accusamus dolorem ex voluptatem optio. Qui et rerum voluptatum officiis itaque itaque.',
		'store_id' => 17,
		'amount' => 40,
	],
	'gift8' => [
		'name' => 'South Dashawnhaven',
		'description' => 'Minima et et voluptas eveniet. Quibusdam aut necessitatibus quo debitis veniam mollitia alias illum. Cum est sed amet architecto. Hic accusamus vel culpa deleniti ullam. Sint libero esse est omnis.',
		'store_id' => 7,
		'amount' => 46,
	],
	'gift9' => [
		'name' => 'McClureborough',
		'description' => 'Vero fugit beatae vero nihil expedita iure quia nisi. Inventore et ipsum consequatur. Quasi impedit fugit non quia dolorem. Cum est ut deserunt quisquam et nobis possimus quod.',
		'store_id' => 7,
		'amount' => 35,
	],
	'gift10' => [
		'name' => 'New Darrenberg',
		'description' => 'Voluptatibus ex hic voluptatem molestias. Odio repellendus nulla dolorem laboriosam cupiditate aut qui.',
		'store_id' => 12,
		'amount' => 21,
	],
	'gift11' => [
		'name' => 'Lamarmouth',
		'description' => 'Itaque dolor sit accusantium quaerat. Possimus qui quo delectus quia necessitatibus. Aut occaecati rerum et repellat odit. Aut quas molestiae doloremque debitis.',
		'store_id' => 17,
		'amount' => 42,
	],
	'gift12' => [
		'name' => 'Eastonfurt',
		'description' => 'Est velit id neque. Voluptas voluptas neque est inventore eligendi. Neque temporibus ullam magni voluptas.',
		'store_id' => 1,
		'amount' => 33,
	],
	'gift13' => [
		'name' => 'East Tyrique',
		'description' => 'Nemo delectus explicabo illum molestias quam dignissimos qui. Consectetur porro provident fugiat et ex. Tenetur iure pariatur ratione laboriosam doloremque in est et.',
		'store_id' => 17,
		'amount' => 21,
	],
	'gift14' => [
		'name' => 'Orinbury',
		'description' => 'Autem magni iusto quibusdam nulla accusamus. Est debitis maxime in nisi. Aut soluta numquam repudiandae quos voluptates. Eaque sed nulla rerum in voluptas.',
		'store_id' => 14,
		'amount' => 33,
	],
	'gift15' => [
		'name' => 'Lake Ambrose',
		'description' => 'Nulla et consectetur impedit voluptatem enim praesentium eum. In explicabo minus temporibus doloribus quis. Quis eum ut voluptas velit.',
		'store_id' => 17,
		'amount' => 17,
	],
	'gift16' => [
		'name' => 'Susanmouth',
		'description' => 'Neque cumque velit autem earum dignissimos. Harum id voluptas earum totam est incidunt tempore voluptates. Molestiae non vel quam laboriosam cupiditate consequatur aperiam.',
		'store_id' => 18,
		'amount' => 45,
	],
	'gift17' => [
		'name' => 'Fredrickburgh',
		'description' => 'Quia eius cupiditate excepturi sit. Maiores et ipsam quis ullam placeat error. Dolor qui iusto eaque sunt in quis.',
		'store_id' => 4,
		'amount' => 17,
	],
	'gift18' => [
		'name' => 'Muellershire',
		'description' => 'Quisquam et nisi quis ratione ut. Eaque eaque et atque ipsam asperiores pariatur eum. Illo voluptas nostrum consequatur minus sapiente laboriosam debitis velit.',
		'store_id' => 2,
		'amount' => 21,
	],
	'gift19' => [
		'name' => 'Krajcikburgh',
		'description' => 'Consequatur quia dolorem et aliquid. Aut quo tempore nemo totam. Qui qui aliquid quia amet esse aut velit. Quisquam modi voluptatem distinctio nobis illum iste hic.',
		'store_id' => 9,
		'amount' => 31,
	],
];
