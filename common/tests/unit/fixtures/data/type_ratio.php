<?php

return [
	'ratio1' => [
		'from' => 0,
		'to' => 1,
		'ratio' => 0.8,
	],
	'ratio2' => [
		'from' => 1,
		'to' => 0,
		'ratio' => 1,
	],
];
