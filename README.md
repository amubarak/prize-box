# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###
* Summary of set up
```
composer install
php init 
(choose Development)
```
* Configurations of server conf path 
```
project path - {{App}}/frontend/web
```
* Database configuration
```
db conf file - common/config/main-local.php
php yii migrate
php yii fixture "*" 
- include account "first_user" 1q2w3e4r5t6y
- data at store
```
* To look info about stores capacity and your gifts and their status 
```
{DOMAIN}/gift/cabinet
```
* If you need some additional data in table
```
php yii fixture/generate-all --count=20
php yii fixture/load Gift
php yii fixture/load Store
php yii fixture/load UserGift
```
* accounts
```
first_user 1q2w3e4r5t6y
```

* how to get email for approve account after registration
```
take your email from 
frontend/runtime/mail
needle looks like 
<p>
    <a href=3D"{DOMAIN}/site/verify-email?token=3DO2Qdkh6XP=W08anAmgSW2QJ8iFk1hIFUG_1591149607">
    {DOMAIN}/site/verify-email=?token=3DO2Qdkh6XPW08anAmgSW2QJ8iFk1hIFUG_1591149607
    </a>
</p>

```

* How to run tests
you need to config codeception
```
php codecept build
php codecept run
```


* Console commands
```
php yii gift/send --count=10 --type=cash  - send gift by type and count
```
