<?php

namespace frontend\models;

class Gift extends BaseGift
{
	const NAME = 'Подарок';
	const POST_SYSTEM = 'giftPostSystem';

	public function rules()
	{
		return [
			[['name', 'amount'], 'required'],
			[['name', 'description', 'amount'], 'safe']
		];
	}

	public function getStore()
	{
		return $this->hasOne(Store::class, ['id' => 'store_id']);
	}

	public function getUserGift()
	{
		return $this->hasOne(UserGift::class, ['gift_id' => 'id']);
	}
}