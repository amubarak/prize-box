<?php

namespace frontend\models;

use yii\db\ActiveRecord;

class UserProfile extends ActiveRecord
{
	public static function tableName()
	{
		return '{{%user_profile}}';
	}

	public function rules()
	{
		return [
			[['user_id', 'gift_id', 'status', 'create_at', 'update_at', 'send_at'], 'safe']
		];
	}


	public function getStore()
	{
		return $this->hasMany(Store::class, ['id' => 'store_id'])
			->viaTable('gift', ['id' => 'gift_id']);
	}
}