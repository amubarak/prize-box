<?php

namespace frontend\models;

use yii\db\ActiveRecord;

/**
 * @property Gift gift
 * @property Store store
 */
class UserGift extends ActiveRecord
{
	public static function tableName()
	{
		return '{{%user_gift}}';
	}

	public function rules()
	{
		return [
			[['user_id', 'gift_id', 'status', 'create_at', 'update_at', 'send_at'], 'safe']
		];
	}

	public function getGift()
	{
		return $this->hasOne(Gift::class, ['id' => 'gift_id']);
	}

	public function getStore()
	{
		return $this->hasMany(Store::class, ['id' => 'store_id'])
			->viaTable('gift', ['id' => 'gift_id']);
	}
}