<?php


namespace frontend\models;


use frontend\interfaces\GiftInterface;
use yii\db\ActiveRecord;

abstract class BaseGift extends ActiveRecord implements GiftInterface
{
	const POST_SYSTEM = '';

	public static function tableName()
	{
		return '{{%gift}}';
	}

	public function __construct($config = [])
	{
		parent::__construct($config);
		$this->name = static::NAME;
	}

	public function setParams(float $amount, int $storeId)
	{
		$this->amount = $amount;
		$this->store_id = $storeId;
	}

}