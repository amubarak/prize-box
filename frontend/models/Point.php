<?php

namespace frontend\models;

class Point extends BaseGift
{
	const NAME = 'Бонусы';
	const POST_SYSTEM = 'appInterface';

	public function rules()
	{
		return [
			[['name', 'amount'], 'required'],
			[['amount'], 'double'],
			[['name', 'amount'], 'safe'],
		];
	}

}