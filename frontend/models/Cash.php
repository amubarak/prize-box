<?php

namespace frontend\models;

use frontend\interfaces\GiftInterface;

class  Cash extends BaseGift implements GiftInterface
{
	const NAME = 'Денюжка';
	const POST_SYSTEM = 'bankBilling';

	public function rules()
	{
		return [
			[['name', 'amount'], 'required'],
			[['amount'], 'double', 'min' => 10, 'max' => 1000],
			[['amount'], 'safe'],
		];
	}


}