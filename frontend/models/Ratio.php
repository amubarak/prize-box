<?php

namespace frontend\models;

use yii\db\ActiveRecord;

class Ratio extends ActiveRecord
{

	public static function tableName()
	{
		return '{{%type_ratio}}';
	}

	public function rules()
	{
		return [
			[['to', 'from', 'ratio'], 'safe']
		];
	}


}