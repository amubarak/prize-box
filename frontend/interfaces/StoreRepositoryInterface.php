<?php


namespace frontend\interfaces;


use frontend\models\Store;
use yii\data\ActiveDataProvider;

interface StoreRepositoryInterface extends BaseRepositoryInterface
{
	public function add(GiftInterface $gift);

	public function changeCapacity(GiftInterface $gift);

	public function all(array $condition = [], array $options = []): ActiveDataProvider;

	public function one(array $condition): Store;

	public function oneNotZeroAmount(array $array);

}