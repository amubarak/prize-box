<?php


namespace frontend\interfaces;


use frontend\models\UserGift;

interface GiftPostServiceInterface
{
	public function send(UserGift $userGift);

	public function sendSeveral(array $userGifts, $condition = []);
}