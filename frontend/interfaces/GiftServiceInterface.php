<?php


namespace frontend\interfaces;


interface GiftServiceInterface
{
	public function add(GiftInterface $gift);

	public function update(GiftInterface $gift);

	public function changeStatus(GiftInterface $gift, int $status);

	public function create(): GiftInterface;

	public function remove(GiftInterface $gift);

	public function oneById(int $id): GiftInterface;

	public function all(array $condition = []);

}