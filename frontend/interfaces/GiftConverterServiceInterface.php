<?php


namespace frontend\interfaces;


interface GiftConverterServiceInterface
{
	public function convert(GiftInterface $gift, int $toType): bool;
}