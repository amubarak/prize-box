<?php


namespace frontend\interfaces;


interface GiftInterface
{
	public function setParams(float $amount, int $storeId);

}