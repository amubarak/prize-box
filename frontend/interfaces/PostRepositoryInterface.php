<?php


namespace frontend\interfaces;


use frontend\models\UserGift;

interface PostRepositoryInterface
{
	public function initiate(UserGift $gift);

	public function checkStatus(UserGift $gift);
}