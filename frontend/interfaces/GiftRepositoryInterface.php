<?php


namespace frontend\interfaces;


use yii\data\ActiveDataProvider;

interface GiftRepositoryInterface extends BaseRepositoryInterface
{
	public function add(GiftInterface $gift);

	public function update(GiftInterface $gift): bool;

	public function remove(GiftInterface $gift);

	public function oneById(int $id): GiftInterface;

	public function all(array $condition = [], array $options = []): ActiveDataProvider;

	public function one(array $condition);

	public function changeStatus(GiftInterface $gift, int $status);

}