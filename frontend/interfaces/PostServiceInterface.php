<?php


namespace frontend\interfaces;


use frontend\models\UserGift;

interface PostServiceInterface
{
	public function initiate(UserGift $userGif);

	public function checkStatus(UserGift $userGif);
}