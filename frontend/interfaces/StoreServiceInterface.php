<?php


namespace frontend\interfaces;


use yii\data\ActiveDataProvider;

interface StoreServiceInterface
{
	public function changeCapacity(GiftInterface $gift);

	public function all(array $condition = []): ActiveDataProvider;

	public function oneByType(int $type);

	public function recoveryCapacity(GiftInterface $gift);
}