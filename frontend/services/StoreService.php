<?php


namespace frontend\services;


use frontend\interfaces\GiftInterface;
use frontend\interfaces\StoreRepositoryInterface;
use frontend\interfaces\StoreServiceInterface;
use frontend\models\Store;
use yii\data\ActiveDataProvider;
use yii\web\UnprocessableEntityHttpException;

/**
 * Class StoreService
 *
 * @property-read StoreRepositoryInterface $repository
 */
class StoreService implements StoreServiceInterface
{

	public $repository;

	public function __construct(StoreRepositoryInterface $repository = null)
	{
		$this->repository = $repository;
	}

	public function changeCapacity(GiftInterface $gift)
	{
		$this->prepareEntity($gift);
		return $this->repository->changeCapacity($gift);
	}

	public function recoveryCapacity(GiftInterface $gift)
	{
		$this->prepareEntity($gift);
		return $this->repository->recoveryCapacity($gift);
	}

	private function prepareEntity(GiftInterface $gift)
	{
		if (!$gift->validate()) {
			throw new UnprocessableEntityHttpException(json_encode($gift->getErrors()));
		};
	}

	public function all(array $condition = []): ActiveDataProvider
	{
		return $this->repository->all($condition);
	}

	public function oneByType(int $type): Store
	{
		return $this->repository->oneNotZeroAmount(['type' => $type]);
	}
}