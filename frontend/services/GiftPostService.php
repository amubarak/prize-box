<?php


namespace frontend\services;


use frontend\enums\GiftStatusEnum;
use frontend\interfaces\GiftPostServiceInterface;
use frontend\models\UserGift;
use Yii;

/**
 * Class GiftPostService
 *
 */
class GiftPostService implements GiftPostServiceInterface
{
	public function send(UserGift $userGift)
	{
		if (Yii::$app->post->initiate($userGift)) {
			Yii::$app->gift->changeStatus($userGift->gift, GiftStatusEnum::SHIPPING);
		};
	}

	public function sendSeveral(array $userGifts, $condition = [])
	{
		foreach ($userGifts as $userGift) {
			if (!($userGift instanceof UserGift)) {
				throw new \InvalidArgumentException('not allowed object for shipping');
			}
			if (Yii::$app->post->initiate($userGift)) {
				Yii::$app->gift->changeStatus($userGift->gift, GiftStatusEnum::SHIPPING);
			};
		}
	}

}