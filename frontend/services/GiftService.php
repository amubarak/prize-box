<?php


namespace frontend\services;


use frontend\components\GiftCreator;
use frontend\interfaces\GiftInterface;
use frontend\interfaces\GiftRepositoryInterface;
use frontend\interfaces\GiftServiceInterface;
use yii\data\ActiveDataProvider;
use yii\web\UnprocessableEntityHttpException;

/**
 * Class GiftService
 *
 * @property-read GiftRepositoryInterface $repository
 */
class GiftService implements GiftServiceInterface
{

	public $repository;

	public function create(): GiftInterface
	{
		$giftCreator = new GiftCreator();
		return $giftCreator->newGift();
	}

	public function add(GiftInterface $gift)
	{
		$this->prepareEntity($gift);
		return $this->repository->add($gift);
	}

	public function all($condition = []): ActiveDataProvider
	{
		return $this->repository->all($condition);
	}

	public function remove(GiftInterface $gift): bool
	{
		$this->prepareEntity($gift);
		return $this->repository->remove($gift);
	}

	public function changeStatus(GiftInterface $gift, int $status): bool
	{
		$this->prepareEntity($gift);
		return $this->repository->changeStatus($gift, $status);
	}

	public function changeType(GiftInterface $gift, int $type): bool
	{
		$this->prepareEntity($gift);
		return $this->repository->changeType($gift, $type);
	}

	public function changeRepository(GiftRepositoryInterface $repository): void
	{
		$this->repository = $repository;
	}

	private function prepareEntity(GiftInterface $gift)
	{
		if (!$gift->validate()) {
			throw new UnprocessableEntityHttpException(json_encode($gift->getErrors()));
		};
	}

	public function oneById(int $id): GiftInterface
	{
		return $this->repository->oneById($id);
	}

	public function update(GiftInterface $gift)
	{
		$this->prepareEntity($gift);
		return $this->repository->update($gift);
	}


}