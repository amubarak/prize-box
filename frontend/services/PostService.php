<?php


namespace frontend\services;


use frontend\interfaces\PostRepositoryInterface;
use frontend\interfaces\PostServiceInterface;
use frontend\models\UserGift;

/**
 * Class PostService
 *
 * @property-read PostRepositoryInterface $repository
 */
class PostService implements PostServiceInterface
{
	public $repository;
	public $postStrategy;

	public function initiate(UserGift $userGift)
	{
		$this->detectSystem($userGift);
		return $this->repository->initiate($userGift);
	}

	public function checkStatus(UserGift $userGift)
	{
		$this->repository->checkStatus($userGift);
	}

	public function detectSystem(UserGift $userGift): void
	{
		$this->repository = $this->postStrategy[$userGift->gift::POST_SYSTEM];
	}
}