<?php


namespace frontend\services;


use frontend\components\exceptions\ChangeCapacityException;
use frontend\components\exceptions\ChangeTypeException;
use frontend\components\exceptions\ConvertForbiddenException;
use frontend\enums\GiftStatusEnum;
use frontend\enums\GiftTypeEnum;
use frontend\interfaces\GiftConverterServiceInterface;
use frontend\interfaces\GiftInterface;
use frontend\models\Ratio;

class GiftConverterService implements GiftConverterServiceInterface
{

	public function convert(GiftInterface $gift, int $toType): bool
	{
		$this->isConvertible($gift, $toType);
		if (\Yii::$app->store->recoveryCapacity($gift)) {
			try {
				$this->includeRatio($gift, $toType);
				$this->changeType($gift, $toType);
				\Yii::$app->store->changeCapacity($gift);
			} catch (ChangeTypeException | ChangeCapacityException | \Exception $e) {
				$this->recoveryCapacity($gift);
				throw new ConvertForbiddenException($e->getMessage());
			}
		};
		return true;
	}

	private function includeRatio(GiftInterface $gift, int $toType)
	{
		$ratioModel = Ratio::find()->where(['from' => $gift->store->type, 'to' => $toType])->one();
		$ratioModel = $ratioModel ?? new Ratio(['to' => 0, 'from' => 1, 'ratio' => 1]);
		$gift->setParams($gift->amount * $ratioModel->ratio, $gift->store_id);
	}

	private function changeType(GiftInterface $gift, int $toType)
	{
		$store = \Yii::$app->store->oneByType($toType);
		if (!$store->hasEnough($gift->amount)) {
			throw new ChangeTypeException('Not enough amount to convert');
		}
		$gift->setParams($gift->amount, $store->id);
		\Yii::$app->gift->update($gift);
	}

	private function recoveryCapacity(GiftInterface $gift)
	{
		$recoveryGift = clone $gift;
		$recoveryGift->setParams(-$gift->amount, $gift->store_id);
		\Yii::$app->store->recoveryCapacity($recoveryGift);
	}

	private function isConvertible(GiftInterface $gift, int $toType)
	{
		$userGiftRecord = $gift->userGift;
		if (empty($userGiftRecord)) {
			throw new ConvertForbiddenException('You cant convert not bounded with user gift');
		}
		if ($userGiftRecord->status != GiftStatusEnum::NEW) {
			throw new ConvertForbiddenException('You cant convert only new gift');
		}
		$store = $gift->store;
		if ($store->type == $toType) {
			throw new ConvertForbiddenException('You cant convert that type to the same');
		}
		$this->checkType($toType);
		$this->checkType($store->type);
	}

	private function checkType($type)
	{
		if (!($type == GiftTypeEnum::CASH || $type == GiftTypeEnum::BONUS_POINTS)) {
			throw new ConvertForbiddenException('Not Allow to convert type');
		}
	}


}