<?php

namespace frontend\repositories\ar;

use frontend\interfaces\GiftInterface;
use frontend\interfaces\StoreRepositoryInterface;
use frontend\models\Store;

class StoreRepository implements StoreRepositoryInterface
{
	public function add(GiftInterface $gift)
	{
		$gift->save();
	}

	public function changeCapacity(GiftInterface $gift)
	{
		// TODO: Implement changeCapacity() method.
	}

	public function oneNotZeroAmount(array $array)
	{
		// TODO: Implement oneNotZeroAmount() method.
	}

	public function all(array $condition = [], array $options = []): array
	{
		// TODO: Implement all() method.
	}

	public function one(array $condition): Store
	{
		// TODO: Implement one() method.
	}
}