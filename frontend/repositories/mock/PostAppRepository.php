<?php

namespace frontend\repositories\mock;

use frontend\interfaces\PostRepositoryInterface;
use frontend\models\UserGift;


class PostAppRepository implements PostRepositoryInterface
{

	public function initiate(UserGift $userGift)
	{
		return true;
	}

	public function checkStatus(UserGift $userGift)
	{
		return true;
	}
}