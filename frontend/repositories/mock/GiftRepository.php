<?php

namespace frontend\repositories\mock;

use frontend\interfaces\GiftInterface;
use frontend\interfaces\GiftRepositoryInterface;


class GiftRepository implements GiftRepositoryInterface
{

	public function save(GiftInterface $gift)
	{
		$gift->save();
	}

	public function add(GiftInterface $gift)
	{
		// TODO: Implement add() method.
	}

	public function remove(GiftInterface $gift)
	{
		// TODO: Implement remove() method.
	}

	public function oneById(int $id): GiftInterface
	{
		// TODO: Implement oneById() method.
	}

	public function all(array $condition = [], array $options = []): array
	{
		// TODO: Implement all() method.
	}

	public function one(array $condition)
	{
		// TODO: Implement one() method.
	}

	public function changeStatus(GiftInterface $gift, int $status)
	{
		// TODO: Implement changeStatus() method.
	}

	public function convert(GiftInterface $gift, $toType): bool
	{
		// TODO: Implement convert() method.
	}
}