<?php

namespace frontend\repositories\mock;

use frontend\interfaces\PostRepositoryInterface;
use frontend\models\UserGift;


class PostBankRepository implements PostRepositoryInterface
{

	public function initiate(UserGift $userGift)
	{
		return true;
	}

	public function checkStatus(UserGift $userGift)
	{
		return true;
	}
}