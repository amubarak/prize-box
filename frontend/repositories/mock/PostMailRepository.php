<?php

namespace frontend\repositories\mock;

use frontend\interfaces\PostRepositoryInterface;
use frontend\models\UserGift;


class PostMailRepository implements PostRepositoryInterface
{

	public function initiate(UserGift $userGift)
	{
		return true;
	}

	public function checkStatus(UserGift $userGift)
	{
		return true;
	}
}