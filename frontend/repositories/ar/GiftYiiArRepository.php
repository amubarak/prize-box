<?php

namespace frontend\repositories\ar;

use frontend\components\exceptions\GiftAlreadySentException;
use frontend\enums\GiftStatusEnum;
use frontend\helpers\ActiveRecordHelper;
use frontend\interfaces\GiftInterface;
use frontend\interfaces\GiftRepositoryInterface;
use frontend\models\Gift;
use frontend\models\UserGift;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Exception as DbException;
use yii\web\NotFoundHttpException;
use yii\web\UnprocessableEntityHttpException;

class GiftYiiArRepository implements GiftRepositoryInterface
{
	public function add(GiftInterface $gift)
	{
		$this->update($gift);
		try {
			$newUserGift = new UserGift();
			$newUserGift->load([
				'user_id' => Yii::$app->user->id,
				'gift_id' => $gift->id,
				'status' => GiftStatusEnum::NEW,
				'update_at' => date(DATE_ISO8601),
			], '');

			return $newUserGift->save();
		} catch (DbException $e) {
			throw new UnprocessableEntityHttpException('Problems UserGift with saving');
		};
	}

	public function update(GiftInterface $gift): bool
	{
		try {
			return $gift->save();
		} catch (DbException $e) {
			throw new UnprocessableEntityHttpException('Problems gift with saving');
		};
	}

	public function remove(GiftInterface $gift)
	{
		try {
			return $this->changeStatus($gift, GiftStatusEnum::CANCELED);
		} catch (NotFoundHttpException $e) {
			$giftRecord = Gift::findOne(['id' => $gift->id]);
			if (!empty($giftRecord)) {
				$giftRecord->delete();
			}
		}
	}

	public function oneById(int $id): GiftInterface
	{
		return $this->one(['id' => $id]);
	}

	public function all(array $condition = [], array $options = []): ActiveDataProvider
	{
		try {
			$giftDataProvider = new ActiveDataProvider([
				'query' => ActiveRecordHelper::createQuery(new UserGift(), $condition, $options)
					->joinWith('store')
			]);
			if ($giftDataProvider->count > 0) {
				return $giftDataProvider;
			}
			throw new NotFoundHttpException();
		} catch (DbException $e) {
			throw new UnprocessableEntityHttpException('Problem with getting gift list');
		}
	}

	public function one(array $condition): GiftInterface
	{
		try {
			$gift = Gift::findOne($condition);
			if (!empty($gift)) {
				return $gift;
			}
			throw new NotFoundHttpException();
		} catch (DbException $e) {
			throw new UnprocessableEntityHttpException('Problem with getting gift');
		}
	}


	public function changeStatus(GiftInterface $gift, int $status)
	{
		$userGiftRecord = $gift->userGift;
		if ($userGiftRecord->status == GiftStatusEnum::SHIPPED) {
			throw new GiftAlreadySentException;
		}
		if (empty($userGiftRecord)) {
			throw new NotFoundHttpException();
		}
		if ($userGiftRecord->status == $status) {
			return false;
		}
		$userGiftRecord->status = $status;
		return $userGiftRecord->save();
	}

}