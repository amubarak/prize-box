<?php

namespace frontend\repositories\ar;

use frontend\components\exceptions\ChangeCapacityException;
use frontend\helpers\ActiveRecordHelper;
use frontend\interfaces\GiftInterface;
use frontend\interfaces\StoreRepositoryInterface;
use frontend\models\Store;
use yii\data\ActiveDataProvider;
use yii\db\Exception as DbException;
use yii\web\NotFoundHttpException;
use yii\web\UnprocessableEntityHttpException;


class StoreYiiArRepository implements StoreRepositoryInterface
{
	public function add(GiftInterface $gift)
	{
		$gift->save();
	}


	public function changeCapacity(GiftInterface $gift)
	{
		$store = $this->one(['id' => $gift->store_id]);
		$gift->setParams($store->getRemains($gift->amount), $store->id);
		$store->changeAmount($gift->amount);
		try {
			return $store->save();
		} catch (DbException $e) {
			throw new ChangeCapacityException();
		}
	}

	public function recoveryCapacity(GiftInterface $gift)
	{
		$store = $this->one(['id' => $gift->store_id]);
		$store->changeAmount(-$gift->amount);
		return $store->save();
	}

	public function all(array $condition = [], array $options = []): ActiveDataProvider
	{
		try {
			$storeDataProvider = new ActiveDataProvider([
				'query' => ActiveRecordHelper::createQuery(new Store(), $condition, $options)
			]);
			if ($storeDataProvider->count > 0) {
				return $storeDataProvider;
			}
			throw new NotFoundHttpException();
		} catch (DbException $e) {
			throw new UnprocessableEntityHttpException($e->getMessage());
		}
	}

	public function one(array $condition): Store
	{
		try {
			$store = ActiveRecordHelper::createQuery(new Store(), $condition)->one();
			if (!empty($store)) {
				return $store;
			}
			throw new NotFoundHttpException();
		} catch (DbException $e) {
			throw new UnprocessableEntityHttpException($e->getMessage());
		}
	}

	public function oneNotZeroAmount(array $condition)
	{
		try {
			$store = ActiveRecordHelper::createQuery(new Store(), $condition)->andWhere(['>', 'amount', '0'])->one();
			if (!empty($store)) {
				return $store;
			}
			throw new NotFoundHttpException();
		} catch (DbException $e) {
			throw new UnprocessableEntityHttpException($e->getMessage());
		}
	}
}