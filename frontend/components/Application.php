<?php


namespace frontend\components;

use frontend\interfaces\GiftConverterServiceInterface;
use frontend\interfaces\GiftPostServiceInterface;
use frontend\interfaces\GiftServiceInterface;
use frontend\interfaces\PostServiceInterface;
use frontend\interfaces\StoreServiceInterface;

/**
 * @property-read GiftServiceInterface $gift
 * @property-read StoreServiceInterface $store
 * @property-read GiftConverterServiceInterface $converter
 * @property-read GiftPostServiceInterface $giftPost
 * @property-read PostServiceInterface $post
 */
class Application extends \yii\web\Application
{

}