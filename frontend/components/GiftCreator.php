<?php


namespace frontend\components;


use frontend\enums\GiftTypeEnum;
use frontend\interfaces\GiftInterface;
use frontend\models\Cash;
use frontend\models\Gift;
use frontend\models\Point;
use frontend\models\Store;
use yii\base\UnknownPropertyException;
use yii\web\NotFoundHttpException;

class GiftCreator
{
	private $amount;
	private $type;
	private $storeId;
	private $giftList = [
		GiftTypeEnum::CASH => Cash::class,
		GiftTypeEnum::GIFT_FROM_LIST => Gift::class,
		GiftTypeEnum::BONUS_POINTS => Point::class,
	];

	public function newGift(): GiftInterface
	{

		$counter = 10;
		do {
			$this->defineType();
			$this->defineAmount();
			$counter--;
		} while ($this->defineStoreRecord() && $counter !== 0);
		if (empty($this->storeId)) {
			throw new UnknownPropertyException('Maybe store is empty');
		}

		return $this->create();
	}

	public function setAmount(int $amount)
	{
		$this->amount = $amount;
	}

	public function setType(GiftTypeEnum $type): void
	{
		$this->type = $type;
	}

	private function defineType(): void
	{
		$this->type = rand(GiftTypeEnum::CASH, GiftTypeEnum::GIFT_FROM_LIST);
	}

	private function defineAmount(): void
	{
		if ($this->type != GiftTypeEnum::GIFT_FROM_LIST) {
			$this->amount = rand(10, 100);
			return;
		}
		$this->amount = 1;
	}


	private function create(): GiftInterface
	{
		/** @var GiftInterface $gift */
		$gift = new $this->giftList[$this->type];
		$gift->setParams($this->amount, $this->storeId);
		return $gift;
	}

	private function defineStoreRecord()
	{
		try {
			/** @var Store $store */
			$store = \Yii::$app->store->oneByType($this->type);
			if (!$store->hasEnough($this->amount)) {
				return true;
			}
			$this->storeId = $store->id;
		} catch (NotFoundHttpException $e) {
			return true;
		}
		return false;

	}
}