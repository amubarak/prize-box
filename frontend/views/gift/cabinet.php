<?php

/**
 * @var $this yii\web\View
 * @var BaseGift $gift
 * @var ActiveDataProvider $giftDataProvider
 * @var ActiveDataProvider $storeDataProvider
 */

$this->title = 'My Yii Application';

use frontend\enums\GiftTypeEnum;
use frontend\helpers\GiftStatusHelper;
use frontend\helpers\GiftTypeHelper;
use frontend\models\BaseGift;
use yii\bootstrap\Html;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Url;

?>
<div class="site-index">
    <div class="jumbotron">
		<?=
		GridView::widget([
			'dataProvider' => $storeDataProvider,
			'columns' => [
				'amount',
				[
					'attribute' => 'type',
					'format' => 'raw',
					'value' => function ($model) {
						return GiftTypeHelper::typeTranslateToString($model->type);
					},
				],
			],
		]) ?>    </div>
    <div class="jumbotron">
		<?=
		GridView::widget([
			'dataProvider' => $giftDataProvider,
			'columns' => [
				[
					'attribute' => 'id',
					'format' => 'raw',
					'value' => function ($model) {
						return $model->gift->id;
					},
				],
				[
					'attribute' => 'type',
					'format' => 'raw',
					'value' => function ($model) {
						return GiftTypeHelper::typeTranslateToString($model->gift->store->type);
					},
				],
				[
					'attribute' => 'amount',
					'format' => 'raw',
					'value' => function ($model) {
						return $model->gift->amount;
					},
				],
				[
					'attribute' => 'amount',
					'format' => 'raw',
					'value' => function ($model) {
						return $model->gift->amount;
					},
				],
				[
					'attribute' => 'status',
					'format' => 'raw',
					'value' => function ($model) {
						return GiftStatusHelper::statusTranslateToString($model->gift->userGift->status);
					},
				],
				[
					'class' => 'yii\grid\ActionColumn',
					'template' => '{convert}',
					'buttons' => [
						'convert' => function ($url, $model, $key) {
							return Html::a('Convert', Url::to(['gift/convert', 'id' => $model->gift->id, 'type' => $model->gift->store->type == GiftTypeEnum::CASH ?? GiftTypeEnum::BONUS_POINTS]));
						},
					]
				],

			],
		]) ?>
    </div>

</div>
</div>
