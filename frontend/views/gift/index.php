<?php

/* @var $this yii\web\View */
/** @var BaseGift $gift */
$this->title = 'My Yii Application';

use frontend\models\BaseGift; ?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Ваш приз это <?= $gift->name ?>!</h1>

        <p class="lead">You Won <?= $gift->amount ?> of them </p>

        <p><a class="btn btn-lg btn-success" href="/gift/refund?id=<?= $gift->id ?>">Вернуть! Не нравится!</a></p>
    </div>

</div>
</div>
