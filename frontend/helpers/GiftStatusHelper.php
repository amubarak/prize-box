<?php


namespace frontend\helpers;


use frontend\enums\GiftStatusEnum;

class GiftStatusHelper
{

	public static function statusTranslateToString(int $status)
	{
		$statuses = GiftStatusEnum::labels();
		if (!in_array($status, $statuses)) {
			throw new \InvalidArgumentException('Status ' . $status . ' not found');
		}
		return array_search($status, $statuses);
	}

}