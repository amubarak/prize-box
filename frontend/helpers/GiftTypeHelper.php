<?php


namespace frontend\helpers;


use frontend\enums\GiftTypeEnum;

class GiftTypeHelper
{
	public static function typeTranslateToInt(string $type)
	{
		$types = GiftTypeEnum::labels();
		if (empty($type)) {
			return GiftTypeEnum::CASH;
		}
		if (!key_exists($type, $types)) {
			throw new \InvalidArgumentException('Type ' . $type . ' not found');
		}
		return $types[$type];
	}

	public static function typeTranslateToString(int $type)
	{
		$types = GiftTypeEnum::labels();
		if (!in_array($type, $types)) {
			throw new \InvalidArgumentException('Type ' . $type . ' not found');
		}
		return array_search($type, $types);
	}

}