<?php

namespace frontend\enums;

class GiftTypeEnum
{
	const CASH = 0;
	const BONUS_POINTS = 1;
	const GIFT_FROM_LIST = 2;

	const CASH_STRING = 'cash';
	const BONUS_POINTS_STRING = 'points';
	const GIFT_FROM_LIST_STRING = 'gifts';

	public static function labels()
	{
		return [
			self::CASH_STRING => self::CASH,
			self::BONUS_POINTS_STRING => self::BONUS_POINTS,
			self::GIFT_FROM_LIST_STRING => self::GIFT_FROM_LIST,
		];
	}
}