<?php

namespace frontend\enums;

class GiftStatusEnum
{
	const NEW = 0;
	const SHIPPING = 1;
	const SHIPPED = 2;
	const CANCELED = 3;

	const NEW_STRING = 'new';
	const SHIPPING_STRING = 'shipping';
	const SHIPPED_STRING = 'shipped';
	const CANCELED_STRING = 'canceled';

	public static function labels()
	{
		return [
			self::NEW_STRING => self::NEW,
			self::SHIPPING_STRING => self::SHIPPING,
			self::SHIPPED_STRING => self::SHIPPED,
			self::CANCELED_STRING => self::CANCELED,
		];
	}
}