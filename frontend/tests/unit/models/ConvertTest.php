<?php

namespace frontend\tests\unit\models;

use common\fixtures\UserFixture;
use frontend\components\exceptions\ConvertForbiddenException;
use frontend\tests\unit\fixtures\GiftFixture;
use frontend\tests\unit\fixtures\StoreFixture;
use frontend\tests\unit\fixtures\TypeRatioFixture;
use frontend\tests\unit\fixtures\UserGiftFixture;
use Yii;


class ConvertTest extends \Codeception\Test\Unit
{
	/**
	 * @var \frontend\tests\UnitTester
	 */
	protected $tester;


	public function _before()
	{
		$this->tester->haveFixtures([
			'user' => [
				'class' => UserFixture::class,
				'dataFile' => codecept_data_dir() . 'user.php'
			],
			'type_ratio' => [
				'class' => TypeRatioFixture::class,
				'dataFile' => __DIR__ . '/../fixtures/data/type_ratio.php'
			],
			'store' => [
				'class' => StoreFixture::class,
				'dataFile' =>  __DIR__ . '/../fixtures/data/store.php'
			],
			'gift' => [
				'class' => GiftFixture::class,
				'dataFile' =>  __DIR__ . '/../fixtures/data/gift.php'
			],
			'user_gift' => [
				'class' => UserGiftFixture::class,
				'dataFile' =>  __DIR__ . '/../fixtures/data/user_gift.php'
			],
		]);
	}

	public function testConvertRight()
	{
		$gift = Yii::$app->gift->oneById(1);
		$this->tester->assertTrue(Yii::$app->converter->convert($gift, 1));
	}

	public function testConvertWrongType()
	{
		$gift = Yii::$app->gift->oneById(2);
		try {
			Yii::$app->converter->convert($gift, 1);
		} catch (ConvertForbiddenException $e) {
			$this->tester->assertEquals('You cant convert that type to the same', $e->getMessage());
		}
	}

	public function testConvertWrongStatus()
	{
		$gift = Yii::$app->gift->oneById(4);
		try {
			Yii::$app->converter->convert($gift, 1);
		} catch (ConvertForbiddenException $e) {
			$this->tester->assertEquals('You cant convert only new gift', $e->getMessage());
		}
	}
}
