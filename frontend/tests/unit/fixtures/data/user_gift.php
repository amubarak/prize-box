<?php

return [
	'user_gift0' => [
		'user_id' => 1,
		'gift_id' => 5,
		'status' => 0,
		'create_at' => '2009-07-26T22:48:23+0400',
	],
	'user_gift1' => [
		'user_id' => 1,
		'gift_id' => 2,
		'status' => 0,
		'create_at' => '2018-07-10T17:43:26+0300',
	],
	'user_gift3' => [
		'user_id' => 1,
		'gift_id' => 15,
		'status' => 0,
		'create_at' => '2005-12-11T16:37:28+0300',
	],

	'user_gift5' => [
		'user_id' => 1,
		'gift_id' => 20,
		'status' => 0,
		'create_at' => '2007-01-01T10:58:56+0300',
	],
	'user_gift6' => [
		'user_id' => 1,
		'gift_id' => 6,
		'status' => 0,
		'create_at' => '1985-03-13T23:39:16+0300',
	],
	'user_gift7' => [
		'user_id' => 1,
		'gift_id' => 18,
		'status' => 0,
		'create_at' => '2015-01-30T16:00:47+0300',
	],
	'user_gift8' => [
		'user_id' => 1,
		'gift_id' => 1,
		'status' => 0,
		'create_at' => '1998-11-24T15:59:14+0300',
	],
	'user_gift10' => [
		'user_id' => 1,
		'gift_id' => 4,
		'status' => 1,
		'create_at' => '2003-06-30T11:09:36+0400',
	],
	'user_gift14' => [
		'user_id' => 1,
		'gift_id' => 7,
		'status' => 0,
		'create_at' => '1993-10-27T18:25:19+0300',
	],
	'user_gift15' => [
		'user_id' => 1,
		'gift_id' => 3,
		'status' => 0,
		'create_at' => '1982-05-30T10:59:33+0400',
	],

];
