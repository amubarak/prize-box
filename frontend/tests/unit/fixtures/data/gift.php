<?php

return [
	'gift0' => [
		'name' => 'Emardport',
		'description' => 'Quisquam cumque quibusdam dolores rerum. Quia culpa debitis rerum maiores. Vitae rem corrupti et dolores.',
		'store_id' => 15,
		'amount' => 21,
	],
	'gift1' => [
		'name' => 'Adrienneborough',
		'description' => 'Tempore nemo minima sequi nam sed. Laudantium esse delectus magnam quasi minus mollitia.',
		'store_id' => 13,
		'amount' => 37,
	],
	'gift2' => [
		'name' => 'Sanfordport',
		'description' => 'Delectus id rem dolorum officiis. Fugit et deserunt et quam ducimus tenetur. Minima rerum laudantium laborum itaque repellendus eum. Ab dolorem esse assumenda nihil ut occaecati.',
		'store_id' => 14,
		'amount' => 22,
	],
	'gift3' => [
		'name' => 'West Augustine',
		'description' => 'Laborum cumque aut labore dolor error. Laborum et maiores aspernatur sint facere.',
		'store_id' => 9,
		'amount' => 39,
	],
	'gift4' => [
		'name' => 'East Jena',
		'description' => 'Iure nihil hic ullam minima itaque praesentium iusto. Eos nam autem corrupti deserunt non qui qui quos. Ut laboriosam est enim totam inventore deserunt nemo. Quia optio accusantium quam voluptate.',
		'store_id' => 2,
		'amount' => 19,
	],
	'gift5' => [
		'name' => 'Port Eleonoremouth',
		'description' => 'Dolorem enim ipsa eveniet dolor eveniet. Quam aut ad vitae esse est est. Sed incidunt accusamus voluptates deleniti officiis qui rerum voluptatum.',
		'store_id' => 14,
		'amount' => 20,
	],
	'gift6' => [
		'name' => 'Konopelskichester',
		'description' => 'Facere deleniti ea nostrum voluptatem cupiditate molestiae ab. Libero tempore beatae eum sint eos et voluptatem. Reprehenderit hic natus voluptas non.',
		'store_id' => 6,
		'amount' => 46,
	],
	'gift7' => [
		'name' => 'Amanifort',
		'description' => 'Itaque mollitia alias tempora fugit unde. Dicta nam soluta et nesciunt et libero. Exercitationem autem dolores fugiat est aut laboriosam labore.',
		'store_id' => 14,
		'amount' => 22,
	],
	'gift8' => [
		'name' => 'North Hildaton',
		'description' => 'Eum quis quasi aspernatur. Eos sunt cumque eius numquam corporis dolore dolores. Vero soluta eius voluptas dolorem temporibus. Corrupti quia dolores sit quam.',
		'store_id' => 11,
		'amount' => 36,
	],
	'gift9' => [
		'name' => 'North Eusebio',
		'description' => 'Explicabo accusantium voluptas est velit autem ut. Dolores eligendi iure omnis. Iure sequi eius minus eaque voluptas molestiae.',
		'store_id' => 10,
		'amount' => 49,
	],
	'gift10' => [
		'name' => 'East Rosamond',
		'description' => 'Animi adipisci rem quis explicabo maxime occaecati. Quis fuga sunt aut occaecati aut. Est impedit sed corporis animi placeat.',
		'store_id' => 18,
		'amount' => 50,
	],
	'gift11' => [
		'name' => 'East Skylarbury',
		'description' => 'Qui quisquam nihil quasi omnis animi voluptatem voluptatem. Eos nulla sequi et sit accusantium culpa et sunt. Ut quia fugit quaerat non quae. A consequatur omnis ea dolorem consequatur qui aperiam.',
		'store_id' => 2,
		'amount' => 17,
	],
	'gift12' => [
		'name' => 'Adamsport',
		'description' => 'Omnis maiores asperiores fuga impedit dolores. Nesciunt sint id omnis minus velit. Ea porro nulla libero consequatur quaerat voluptas.',
		'store_id' => 11,
		'amount' => 32,
	],
	'gift13' => [
		'name' => 'South Kiley',
		'description' => 'Aliquam commodi numquam officiis ut. Ut repellendus amet sit dolor voluptatem. Iste cumque sed est expedita. Nemo velit rerum tempore praesentium perferendis totam.',
		'store_id' => 9,
		'amount' => 33,
	],
	'gift14' => [
		'name' => 'Jacobsonstad',
		'description' => 'In maxime sunt laborum ut adipisci. Natus quia ratione nobis neque. Non quaerat ea asperiores inventore quod possimus dolor fugit. Sit facere ipsam fugit nostrum exercitationem.',
		'store_id' => 13,
		'amount' => 28,
	],
	'gift15' => [
		'name' => 'Lehnerville',
		'description' => 'Dolor quis qui non. Quo esse aperiam voluptatem aliquid magni eos necessitatibus. A quisquam adipisci eum et ad harum hic. Impedit aut eum voluptate recusandae.',
		'store_id' => 16,
		'amount' => 21,
	],
	'gift16' => [
		'name' => 'Port Kearaport',
		'description' => 'Ut consequatur praesentium consequatur qui itaque nemo. Ad deleniti beatae maiores. Dignissimos incidunt deserunt quam a officiis eligendi non. Voluptate officia repudiandae rem quis tempora.',
		'store_id' => 5,
		'amount' => 28,
	],
	'gift17' => [
		'name' => 'Robertoberg',
		'description' => 'Tenetur tempore in quas aut a asperiores. Numquam odit dolor iusto esse quos magni cupiditate illo. Quia quibusdam quaerat ea.',
		'store_id' => 5,
		'amount' => 40,
	],
	'gift18' => [
		'name' => 'Lake Francisco',
		'description' => 'Et sunt quibusdam quia tempora consequuntur. Vel maxime nesciunt et inventore minus est sed et. Qui magni est voluptate qui explicabo.',
		'store_id' => 4,
		'amount' => 37,
	],
	'gift19' => [
		'name' => 'Lake Issac',
		'description' => 'Molestiae earum nisi fugit optio sint iste soluta. Nesciunt velit voluptas et distinctio eligendi. Totam consectetur sed reprehenderit delectus repudiandae aut.',
		'store_id' => 16,
		'amount' => 37,
	],
];
