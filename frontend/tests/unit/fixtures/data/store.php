<?php

return [
	'store0' => [
		'amount' => 22,
		'type' => 0,
		'update_at' => '1989-10-30T01:33:23+0300',
	],
	'store1' => [
		'amount' => 721,
		'type' => 1,
		'update_at' => '2014-08-14T11:42:36+0400',
	],
	'store2' => [
		'amount' => 137,
		'type' => 1,
		'update_at' => '1998-04-15T12:29:09+0400',
	],
	'store3' => [
		'amount' => 698,
		'type' => 1,
		'update_at' => '1987-04-10T09:01:29+0400',
	],
	'store4' => [
		'amount' => 981,
		'type' => 0,
		'update_at' => '2018-09-08T21:42:51+0300',
	],
	'store5' => [
		'amount' => 18,
		'type' => 2,
		'update_at' => '1984-05-03T00:34:35+0400',
	],
	'store6' => [
		'amount' => 121,
		'type' => 2,
		'update_at' => '2014-02-12T21:38:49+0400',
	],
	'store7' => [
		'amount' => 841,
		'type' => 2,
		'update_at' => '1986-12-17T14:25:52+0300',
	],
	'store8' => [
		'amount' => 370,
		'type' => 1,
		'update_at' => '2000-05-12T15:57:18+0400',
	],
	'store9' => [
		'amount' => 602,
		'type' => 0,
		'update_at' => '1973-06-16T03:55:16+0300',
	],
	'store10' => [
		'amount' => 75,
		'type' => 2,
		'update_at' => '1989-06-10T10:53:42+0400',
	],
	'store11' => [
		'amount' => 103,
		'type' => 0,
		'update_at' => '1977-11-12T07:52:53+0300',
	],
	'store12' => [
		'amount' => 624,
		'type' => 1,
		'update_at' => '1990-07-17T17:14:09+0400',
	],
	'store13' => [
		'amount' => 319,
		'type' => 0,
		'update_at' => '2001-08-18T00:53:46+0400',
	],
	'store14' => [
		'amount' => 571,
		'type' => 0,
		'update_at' => '1982-09-10T15:16:44+0400',
	],
	'store15' => [
		'amount' => 889,
		'type' => 0,
		'update_at' => '2009-09-30T16:01:21+0400',
	],
	'store16' => [
		'amount' => 150,
		'type' => 2,
		'update_at' => '1980-12-14T08:16:07+0300',
	],
	'store17' => [
		'amount' => 899,
		'type' => 1,
		'update_at' => '2016-07-24T09:52:36+0300',
	],
	'store18' => [
		'amount' => 228,
		'type' => 2,
		'update_at' => '2017-02-03T18:06:23+0300',
	],
	'store19' => [
		'amount' => 694,
		'type' => 2,
		'update_at' => '1985-03-29T11:16:12+0300',
	],
];
