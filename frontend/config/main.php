<?php

use frontend\repositories\ar\GiftYiiArRepository;
use frontend\repositories\ar\StoreYiiArRepository;
use frontend\services\GiftConverterService;
use frontend\services\GiftService;
use frontend\services\StoreService;

$params = array_merge(
	require __DIR__ . '/../../common/config/params.php',
	require __DIR__ . '/../../common/config/params-local.php',
	require __DIR__ . '/params.php',
	require __DIR__ . '/params-local.php'
);

return [
	'id' => 'app-frontend',
	'basePath' => dirname(__DIR__),
	'bootstrap' => ['log'],
	'controllerNamespace' => 'frontend\controllers',
	'components' => [
		'request' => [
			'csrfParam' => '_csrf-frontend',
		],
		'user' => [
			'identityClass' => 'common\models\User',
			'enableAutoLogin' => true,
			'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
		],
		'session' => [
			'name' => 'advanced-frontend',
		],
		'log' => [
			'traceLevel' => YII_DEBUG ? 3 : 0,
			'targets' => [
				[
					'class' => 'yii\log\FileTarget',
					'levels' => ['error', 'warning'],
				],
			],
		],
		'errorHandler' => [
			'errorAction' => 'site/error',
		],

		'urlManager' => [
			'enablePrettyUrl' => true,
			'showScriptName' => false,
			'rules' => [
			],
		],
		'gift' => [
			'class' => GiftService::class,
			'repository' => new GiftYiiArRepository(),
		],
		'store' => [
			'class' => StoreService::class,
			'repository' => new StoreYiiArRepository(),
		],
		'converter' => [
			'class' => GiftConverterService::class,
		]
	],
	'params' => $params,
];
