<?php

namespace frontend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;


class GiftController extends Controller
{

	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::class,
				'only' => ['index','convert', 'refund', 'cabinet'],
				'rules' => [
					[
						'actions' => ['index', 'convert', 'refund', 'cabinet'],
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
			'verbs' => [
				'class' => VerbFilter::class,
				'actions' => [
					'index' => ['GET'],
					'cabinet' => ['GET'],
				],
			],
		];
	}

	public function actions()
	{
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
			'captcha' => [
				'class' => 'yii\captcha\CaptchaAction',
				'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
			],
		];
	}

	public function actionIndex()
	{
		try {
			$gift = Yii::$app->gift->create();
			Yii::$app->store->changeCapacity($gift);
			try {
				Yii::$app->gift->add($gift);
			} catch (\Exception $e) {
				Yii::$app->store->recoveryCapacity($gift);
				Yii::$app->gift->remove($gift);
				Yii::$app->session->setFlash('Error', $e->getMessage());
				return $this->redirect('/gift/cabinet');
			}
		} catch (\Exception $e) {
			$message = YII_ENV_PROD ? 'Get gift error' : 'Error' . $e->getMessage();
			Yii::$app->session->setFlash('Error', $message);
			return $this->redirect('/');
		}
		return $this->render('index', [
			'gift' => $gift,
		]);
	}

	public function actionRefund(int $id)
	{
		try {
			$gift = Yii::$app->gift->oneById($id);
			if (Yii::$app->gift->remove($gift)) {
				Yii::$app->store->recoveryCapacity($gift);
			};
			Yii::$app->session->setFlash('Success', 'Вернули!');
			return $this->redirect('/gift/cabinet');
		} catch (\Exception $e) {
			$message = YII_ENV_PROD ? 'Refund error' : 'Error' . $e->getMessage();
			Yii::$app->session->setFlash('Error', $message);
			return $this->redirect('/');
		}
	}

	public function actionConvert(int $id, int $type)
	{
		try {
			$gift = Yii::$app->gift->oneById($id);
			Yii::$app->converter->convert($gift, $type);
			Yii::$app->session->setFlash('Success', 'Конвертнули!');
			return $this->redirect('/gift/cabinet');
		} catch (\Exception $e) {
			$message = YII_ENV_PROD ? 'Convert error' : 'Error: ' . $e->getMessage();
			Yii::$app->session->setFlash('Error', $message);
			return $this->redirect('/gift/cabinet');
		}
	}

	public function actionCabinet()
	{
		try {
			$storeDataProvider = Yii::$app->store->all(['>', 'amount', 0]);
			$giftDataProvider = Yii::$app->gift->all(['user_id' => Yii::$app->user->id]);
		} catch (\Exception $e) {
			$message = YII_ENV_PROD ? 'Get gift error' : 'Error' . $e->getMessage();
			Yii::$app->session->setFlash('Error', $message);
			return $this->redirect('/');
		}
		return $this->render('cabinet', [
			'giftDataProvider' => $giftDataProvider,
			'storeDataProvider' => $storeDataProvider
		]);
	}

}
