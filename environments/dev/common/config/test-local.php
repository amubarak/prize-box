<?php
return [
	'components' => [
		'db' => [
			'dsn' => 'pgsql:host=localhost;dbname=prize_box',
			'schemaMap' => [
				'pgsql' => [
					'class' => 'yii\db\pgsql\Schema',
					'defaultSchema' => 'test',
				],
			],
			'on afterOpen' => function ($event) {
				$event->sender->createCommand("SET search_path TO test;")->execute();
			},
		],
	],
];
